const baseUrl = "https://k8s.omoda.nl";

window.ENV = {
  "API_BASE_URL": baseUrl,
  "API_URL": `${baseUrl}/external-fulfilment`,
  "AUTH_API_URL": `${baseUrl}/auth`,
  "HTTPS_URL_REWRITE": true
}
