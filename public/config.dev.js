window.ENV = {
  "API_BASE_URL": "http://localhost:8080",
  "API_URL": "http://localhost:8080",
  "AUTH_API_URL": "https://k8s-a.omoda.nl/auth",
  "HTTPS_URL_REWRITE": false
}
