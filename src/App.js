import React from 'react';
import './App.css';
import MainLayout from './layouts/MainLayout';
import {BrowserRouter as Router} from 'react-router-dom';
import 'react-table/react-table.css';

function App() {
  return (
    <Router>
      <MainLayout/>
    </Router>
  );
}

export default App;
