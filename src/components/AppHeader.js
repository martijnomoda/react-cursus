import React from 'react';
import {withRouter} from 'react-router-dom';
import {Navbar, NavbarBrand} from 'reactstrap';

class AppHeader extends React.Component {

  render() {

    return (
        <div>
          <Navbar dark color="dark" expand="md" className="oef-header mb-4">
            <NavbarBrand href="/">Omoda Test</NavbarBrand>
          </Navbar>
        </div> :
        null
    );
  }
}

export default withRouter(AppHeader);
