import React, { Component } from 'react';
import {Route, Switch, withRouter} from 'react-router-dom'; 
import '../App.css';
import HomePage from '../pages/HomePage';
import AppHeader from "../components/AppHeader";
import {Container} from "reactstrap";

class MainLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }


  render() {

    return (
      <div>
        <AppHeader />
        <Container fluid>
          <main>
            <Switch>
              <Route path='/' render={() => <HomePage />}/>
            </Switch>
          </main>
        </Container>
      </div>
    );
  }
}

export default withRouter(MainLayout);