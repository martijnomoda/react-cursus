# Omoda todolist-applicatie

1. Installeer en run de applicatie
2. Verander de naam van de applicatie van "Omoda Test" naar "Omoda Todolist". Zorg ervoor dat dit zowel in de header veranderd als in de browserbalk
3. Zorg ervoor dat de header van de applicatie een rode achtergrond krijgt.
4. Plaats op de homepage een tabel met een voorbeeld todo's. Je kunt hiervoor het component react-table gebruiken. Deze is al geinstalleerd in de applicatie (Voor voorbeelden, zie: https://www.javatpoint.com/react-table). De tabel bevat de volgende velden: Uitvoerder, Datum, Omschrijving.
5. Voeg een knop toe onder de Todolijst waarbij je een nieuwe todo kan aanmaken. Als je hier op klikt, moeten er invoervelden en een submitknop verschijnen onder de todolijst. Gebruik hiervoor de componenten van Reactstrap (Button, Input)
6. Verplaats de knop en de inputs in een apart component. Als je de nieuwe Todo submit, moet deze via de props van het component worden toegevoegd aan de Reactlist die in stap 4 is gemaakt. 
7. Zorg dat er vanuit de Homepage methode die in stap6 is gemaakt (addTodoItem)  het TodoForm component wordt geupdated middels een property. Toon dan in het formulier-component een melding dat het toevoegen is gelukt en wis de invoervelden. Gebruik hiervoor de componentDidUpdate methode van React.  

